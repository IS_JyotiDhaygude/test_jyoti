﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Web_API_CRUD.Models;

namespace Web_API_CRUD.Controllers
{
    public class storesController : ApiController
    {
        private testEntities db = new testEntities();

        // GET: api/stores
        public IQueryable<store> Getstores()
        {
            return db.stores;
        }

        // GET: api/stores/5
        [ResponseType(typeof(store))]
        public IHttpActionResult Getstore(int id)
        {
            store store = db.stores.Find(id);
            if (store == null)
            {
                return NotFound();
            }

            return Ok(store);
        }

        // PUT: api/stores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putstore(int id, store store)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != store.Store_Id)
            {
                return BadRequest();
            }

            db.Entry(store).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!storeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/stores
        [ResponseType(typeof(store))]
        public IHttpActionResult Poststore(store store)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.stores.Add(store);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (storeExists(store.Store_Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = store.Store_Id }, store);
        }

        // DELETE: api/stores/5
        [ResponseType(typeof(store))]
        public IHttpActionResult Deletestore(int id)
        {
            store store = db.stores.Find(id);
            if (store == null)
            {
                return NotFound();
            }

            db.stores.Remove(store);
            db.SaveChanges();

            return Ok(store);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool storeExists(int id)
        {
            return db.stores.Count(e => e.Store_Id == id) > 0;
        }
    }
}